# On Budget
> menurut saya kebanyakan industry software tidak mendetermine lebih detail soal requirement dan needs dari apps developement
misalnya untuk membangun sebuah mobile / front end apps;
kebanyakan industry hanya menyiapkan budget untuk developernya saja tanpa memikirkan orang yang akan mendesign ui dari apps tersebut atau orang yang akan melakukan research dari setiap experiencenya user. Kebanyakan industry software menggambungkan tanggung jawab dari ui desginer dan ux researcher dengan frontend/mobile developer sehingga budget yang diperkirakan hanya untuk developernya saja, sementara untuk mendesign sebuah ui harus disiapkan resource yang memadai untuk mensupport kegiatan design contohnya laptop/pc/mac yang mempunyai spesifikasi yang worth it.

> masalah lainya juga ada overbudget yang dihabiskan untuk resource yang tidak efisien, misalnya dalam penyediaan server atau network;
kadang industry membeli resource yang physical, yang memang kita bisa lihat fisiknya serta tanpa harus ada internet connection untuk cofigurasinya, tapi industry tidak memikirkan untuk scale up nya, gimana kalo resourcenya perlu di update? apakah harus membeli resource yang baru?
Menurut saya sudah saatnya industry software untuk menggukan cloud platform entah saas, paas, iaas ataupun daas. Scale up/down tinggal geser (depends on budget) 


# On Timeline
> mungkin saja tidak mencapai timeline karena minimnya koordinasi dan teamwork, biasanya kegagalan seperti ini akibat dari legacy development, tanpa menggunakan tools dan concept untuk mempercepat dan mendeliver minim error apps
misalnya;
development tanda test driven, yang mengakibatkan perubahan dalam tahap pengembangan
concept yang harus diterapkan untuk meng avoid deliver telat yaitu SCRUM, setiap hari kita bisa mengontrol apa yang akan di kerjakan, yang sudah, dan belum. dan juga kita bisa tau apakah kelemahan dan kelebihan dari developernya dalam artian mengerjakan task yang diberikan, bukan berarti developer itu tidak kompetent tapi dengan begitu kita bisa tau dimana tempat yang suitble buat dia


