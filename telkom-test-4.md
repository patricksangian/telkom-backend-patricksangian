# adventage and disadventage of using non-blocking I/O

* advantage
> seperti yg kita tahu, non blocking itu di implementasikan di javascript, dengan non blocking ini kita dapat membuat suatu fungsi(callback/promise) dimana fungsi itu sendiri menunggu sampai fungsi yang lain selesai (await). Keuntungannya yaitu:
- mengimplementasikan threads like pada javascript (meskipun js adalah single thread)
- less code dibandingkan multi threads yang ada di python, java, c++, etc. Di tulis direct pada 1 block of code


* disadvantage
> membingungkan jika kebanyakan callback / promise (callback hell)