# record the error on production

> Menurut saya dengan menambakan error_log pada setiap Error Exception, yang di masukkan kedalam record adalah instance dari Error Exception Class, user_token atau user_id, timestamp activity, dan request header dari user device / browser (os, browser_type)

note: implementasi error handler di production tidak boleh menamplikan debug untuk alasan security dan user experience, at least send error message berupa pop up atau alert