import math


def count_sheets_of_rupiah():

    input_value = float()
    rupiahs_value = [100000, 50000, 20000,
                     10000, 5000, 2000, 1000, 500, 200, 100]
    results_money = dict()

    # try to check if the input is a valid rupiahs value
    try:
        input_value = math.ceil(
            float(input("Please, Input value of money! \n")))
    except ValueError:
        # get the thrown exception and return to rupiahs value input activity
        print("Your input is not a valid money value. Please input again. \n")
        return count_sheets_of_rupiah()

    for rp_value in rupiahs_value:
        if input_value < 100:
            input_value = 100  # set input_value to round up to 100 rupiahs if it's below than 100

        count = input_value // rp_value  # count the sheet(s) of rupiahs value

        if count > 0:

            # set the payload of sheet(s) count
            sheets = {
                'Rp. {:,}'.format(rp_value): int(count)
            }

            # put the count result to the collection or dictionary
            results_money.update(sheets)

            # set input value as the result of input_value mod each of rupiahs value
            input_value = input_value % rp_value
        else:
            continue

    return results_money


results = count_sheets_of_rupiah()
print(results)
