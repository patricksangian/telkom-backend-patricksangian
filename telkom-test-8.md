# instruction
# a. database dan tipe data
# b. adventages and disadventage
# c. query in 5 depth reverse


## answer

## soal a
* Table Personal
    * _id int() auto_increment
    * nik varchar(255) unique null
    * name varchar(255)
    * blood_type enum('O','A','B','AB')
    * place_of_birth varchar(255)
    * date_of_birth date()
    * gender enum('Male','Female')
    * address text()
    * marital_status enum('married','single','widow','widower',' divorce')
    * job varchar(255)
    * id_personal_family_registration int() unsigned()
    * create_at timestamp()
    * update_at timestamp()

* Table Family_Registration
    * _id int() auto_increment
    * family_registration_no varchar(255)
    * create_at timestamp()
    * update_at timestamp()

* Table Person_on_Family_Registration
    * _id int() auto_increment
    * nik varchar(255) null
    * family_registration_no varchar(255)
    * family_relation_type enum('leader','spouse','children','parent','in-laws')
    * create_at timestamp()
    * update_at timestamp()

## soal b
* kelebihan
    * terstruktur karena menggunakan many to many relationship
    * penulisan kolom yang jelas
    * dibuat dalam bentuk SQL karena mengutamakan relationship

* kekurangan
    * allocation memory untuk tipe data varchar yang sulit diprediksi karena setiap data personal adalah unique, sehingga beberapa field yang tipe data varchar harus di allocated maximum, dampak nya ada pemborosan resource/storage
    * tipe data enum akan memakan allocation storage yang besar, akan lebih baik diganti ke varchar() tapi saat insertion di backend enum dijadikan class selection sesuai bahasa pemrogramman yang digunakan dengan begitu bisa dijadikan keuntungan pada saat refactor (penambahan/pengurangan selection type)

## soal c
* Query (ex: MySQL)

    with recursive last_five_family_generation as (
        select 
            p.*, 
            count(*) as counted_genration,
            pfr.family_registration_no
            pfr2.nik as leader_nik
        from Personal p
        join Person_on_Family_Registration pfr on p.nik = pfr.nik
        join (
            select 
                top 1 nik 
            from Person_on_Family_Registration as pfr_joined
            where pfr.family_registration_no = pfr_joined
        ) pfr2

        union all

        select 
            p.*, 
            pfr.family_registration_no
            pfr2.nik as leader_nik
        from Personal p
        join Person_on_Family_Registration pfr on p.nik = pfr.nik
        join (
            select 
                top 1 nik 
            from Person_on_Family_Registration as pfr_joined
            where pfr.family_registration_no = pfr_joined
        ) pfr2

        where last_five_family_generation.leader_nik
    )

    select * from last_five_family_generation wher counted_genration <= 5








