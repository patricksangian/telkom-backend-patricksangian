# soal a
> karena non blocking function yang bersifat time atau interval time tidak bisa dijalankan dalam perulangan (for,while, each, dan foreach)

#soal b
> dengan mejadikan code tersebut menjadi recursive function
function iterate(no_of_iteration) {
    if (no_of_iteration == 0) {
        return 0
    }
    console.log("Iteration #" + (no_of_iteration - 1))
    setTimeout(() => {
        iterate(no_of_iteration - 1)
    }, 1000)
}

setTimeout(() => {
    iterate(4)
}, 1000)