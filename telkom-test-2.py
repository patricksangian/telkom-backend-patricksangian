def sort_with_swap():
    data_list = [14, 8, 3, 22, 9]
    len_of_data = len(data_list)
    count_of_swap = 0

    for i in range(len_of_data):
        min_index = i
        for x in range(i+1, len_of_data):
            if data_list[min_index] > data_list[x]:
                min_index = x

        data_list[i], data_list[min_index] = data_list[min_index], data_list[i]
        count_of_swap += 1

    return data_list, count_of_swap


data = sort_with_swap()
print("Count of swap: {}".format(data[1]))
print("Collection of data: \n")
print(data[0])
